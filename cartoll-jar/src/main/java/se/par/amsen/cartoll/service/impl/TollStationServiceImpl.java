package se.par.amsen.cartoll.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.par.amsen.cartoll.domain.TollStation;
import se.par.amsen.cartoll.repository.TollStationRepository;
import se.par.amsen.cartoll.service.TollStationService;

@Service
public class TollStationServiceImpl implements TollStationService{

	@Autowired
	private TollStationRepository repository;
	
	@Override
	public long createTollStation(TollStation tollStation) {
		return repository.createTollStation(tollStation);
	}

	@Override
	public List<TollStation> getTollStations() {
		return repository.getTollStations();
	}

	@Override
	public TollStation getTollStationById(long id) {
		return repository.getTollStationById(id);
	}

	@Override
	public long updateTollStation(TollStation tollStation) {
		return repository.updateTollStation(tollStation);
	}

	@Override
	public boolean removeTollStation(long id) {
		return repository.removeTollStationById(id);
	}

	@Override
	public void clearAllTollStations() {
		repository.clearAllTollStations();
	}
}
