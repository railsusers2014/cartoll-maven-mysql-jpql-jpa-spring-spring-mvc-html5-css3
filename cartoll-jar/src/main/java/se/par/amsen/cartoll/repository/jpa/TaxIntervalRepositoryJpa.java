package se.par.amsen.cartoll.repository.jpa;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import se.par.amsen.cartoll.domain.TaxInterval;
import se.par.amsen.cartoll.repository.TaxIntervalRepository;

public class TaxIntervalRepositoryJpa extends AbstractRepositoryJpa implements TaxIntervalRepository {

	@Transactional
	@Override
	public long createTaxInterval(TaxInterval taxInterval) {
		em.persist(taxInterval);
		return taxInterval.getId();
	}

	@Transactional
	@Override
	public long updateTaxInterval(TaxInterval taxInterval) {
		return em.merge(taxInterval).getId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TaxInterval> getTaxIntervals() {
		return em.createNamedQuery("getAllTaxIntervals").getResultList();
	}

	@Override
	public TaxInterval getTaxIntervalById(long id) {
		return em.find(TaxInterval.class, id);
	}

	@Transactional
	@Override
	public boolean removeTaxIntervalById(long id) {
		em.remove(getTaxIntervalById(id));
		return em.find(TaxInterval.class, id) == null;
	}
	
	@Transactional
	@Override
	public void clearAllTaxIntervals() {
		em.createNamedQuery("clearAllTaxIntervals").executeUpdate();
	}

}
