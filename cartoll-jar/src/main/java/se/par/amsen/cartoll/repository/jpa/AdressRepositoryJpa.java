package se.par.amsen.cartoll.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import se.par.amsen.cartoll.domain.Adress;
import se.par.amsen.cartoll.repository.AdressRepository;

public class AdressRepositoryJpa extends AbstractRepositoryJpa implements AdressRepository {

	@Transactional
	@Override
	public long createAdress(Adress adress) {
		em.persist(adress);
		return adress.getId();
	}

	@Override
	public long updateAdress(Adress adress) {
		return em.merge(adress).getId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Adress> getAdresses() {
		return em.createNamedQuery("getAllAdresses").getResultList();
	}

	@Override
	public Adress getAdressById(long id) {
		return em.find(Adress.class, id);
	}

	@Transactional
	@Override
	public boolean removeAdressById(long id) {
		em.remove(getAdressById(id));
		return em.find(Adress.class, id) == null;
	}

	@Transactional
	@Override
	public void clearAllAdresses() {
		em.createNamedQuery("clearAllAdresses").executeUpdate();
	}

}
