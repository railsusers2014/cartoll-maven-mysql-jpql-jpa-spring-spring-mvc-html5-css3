package se.par.amsen.cartoll.repository.jpa;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import se.par.amsen.cartoll.domain.Adress;
import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.vehicle.Vehicle;
import se.par.amsen.cartoll.repository.AdressRepository;
import se.par.amsen.cartoll.repository.OwnerRepository;
import se.par.amsen.cartoll.repository.VehicleRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/application_context_mock_jpa.xml"})
public class OwnerRepositoryJpaTest {
	
	Owner owner;
	Adress adress;
	
	@Autowired
	OwnerRepository repo;
	
	@Autowired
	AdressRepository adressRepo;
	
	@Autowired
	VehicleRepository vehicleRepo;

	@Before
	public void setUp() throws Exception {
		vehicleRepo.clearAllVehicles();
		repo.clearAllOwners();
		adressRepo.clearAllAdresses();
		adress = adressRepo.getAdressById(adressRepo.createAdress(new Adress("","","")));
		
		owner = new Owner("Linnea", "Amsen", "851122XXXX", adress, new ArrayList<Vehicle>());
	}
	
	@After
	public void tearDown() {
		vehicleRepo.clearAllVehicles();
		repo.clearAllOwners();
		adressRepo.clearAllAdresses();
	}

	@Test
	public void testCreateOwner() {
		assertEquals("Owner id is set", true, repo.createOwner(owner) > 0);
	}

	@Test
	public void testGetOwners() {
		repo.createOwner(new Owner("Linnea", "Amsen", "851122XXXX", adress, null));
		repo.createOwner(new Owner("Coolio", "Foolio", "133722XXXX", adress, null));
		repo.createOwner(new Owner("Roberto", "Tornbergio", "861122XXXX", adress, null));
		
		assertEquals("No. of Owners are 3", 3, repo.getOwners().size());
	}

	@Test
	public void testGetOwnerById() {
		repo.createOwner(owner);
		assertEquals("Get Owner", owner.getId(), repo.getOwnerById(owner.getId()).getId());
	}
	
	@Test
	public void testUpdateOwner() {
		repo.createOwner(owner);
		owner.setFirstName("Korv");
		repo.updateOwner(owner);
		
		assertEquals("Owner is updated", "Korv", repo.getOwnerById(owner.getId()).getFirstName());
	}

	@Test
	public void testRemoveOwnerById() {
		repo.createOwner(owner);
		repo.removeOwnerById(owner.getId());
		assertEquals("Owner removed", 0, repo.getOwners().size());
	}

}
