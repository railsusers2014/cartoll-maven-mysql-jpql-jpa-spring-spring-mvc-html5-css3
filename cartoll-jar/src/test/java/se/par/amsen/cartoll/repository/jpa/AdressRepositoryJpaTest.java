package se.par.amsen.cartoll.repository.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import se.par.amsen.cartoll.domain.Adress;
import se.par.amsen.cartoll.repository.AdressRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/application_context_mock_jpa.xml"})
public class AdressRepositoryJpaTest {

	Adress adress = new Adress("Coolio", "CoolTown", "FoolCode");
	
	@Autowired
	AdressRepository repo;
	
	@Before
	public void setUp(){
		repo.clearAllAdresses();
	}
	
	@After
	public void tearDown() {
		repo.clearAllAdresses();
	}

	@Test
	public void testCreateAdress() {
		assertEquals("Adress id is set", true, repo.createAdress(adress) > 0);
	}

	@Test
	public void testGetAdresses() {
		repo.createAdress(new Adress("a","b","c"));
		repo.createAdress(new Adress("b","c","d"));
		repo.createAdress(new Adress("c","d","e"));
		
		assertEquals("No. of adresses are 3", 3, repo.getAdresses().size());
	}

	@Test
	public void testGetAdressById() {
		repo.createAdress(adress);
		assertEquals("Get adress", adress.getId(), repo.getAdressById(adress.getId()).getId());
	}

	@Test
	public void testRemoveAdressById() {
		repo.createAdress(adress);
		repo.removeAdressById(adress.getId());
		assertEquals("Adress removed", 0, repo.getAdresses().size());
	}

}
