package se.par.amsen.cartoll.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PriceIntervalTest {

	private TaxInterval interval1;
	private TaxInterval interval2;
	
	private static final int HOUR_FROM_1 = 8;
	private static final int MINUTE_FROM_1 = 30;
	private static final int HOUR_TO_1 = 14;
	private static final int MINUTE_TO_1 = 59;
	private static final TaxLevel TAX_LEVEL_1 = TaxLevel.HIGH;
	
	private static final int HOUR_FROM_2 = 3;
	private static final int MINUTE_FROM_2 = 0;
	private static final int HOUR_TO_2 = 3;
	private static final int MINUTE_TO_2 = 59;
	private static final TaxLevel TAX_LEVEL_2 = TaxLevel.LOW;
	
	private static final int HOUR_TEST_1 = 9;
	private static final int MINUTE_TEST_1 = 0;
	
	private static final int HOUR_TEST_2 = 3;
	private static final int MINUTE_TEST_2 = 30;
	

	
	@Before
	public void setup() {
		interval1 = new TaxInterval(HOUR_FROM_1,MINUTE_FROM_1,HOUR_TO_1,MINUTE_TO_1,TAX_LEVEL_1);
		interval2 = new TaxInterval(HOUR_FROM_2,MINUTE_FROM_2,HOUR_TO_2,MINUTE_TO_2,TAX_LEVEL_2);
	}
	
	@Test
	public void testIntervals() {
		assertEquals("Time is within interval 1", true, interval1.isInInterval(HOUR_TEST_1, MINUTE_TEST_1));
		assertEquals("Time is not within interval 1", false, interval1.isInInterval(HOUR_TEST_2, MINUTE_TEST_2));
		
		assertEquals("Time is within interval 2", true, interval2.isInInterval(HOUR_TEST_2, MINUTE_TEST_2));
		assertEquals("Time is not within interval 2", false, interval2.isInInterval(HOUR_TEST_1, MINUTE_TEST_1));
	}
	
	@Test
	public void testPriceLevels() {
		assertEquals("Price level test 1", TAX_LEVEL_1, interval1.getTaxLevel());
		assertEquals("Price level test 2", TAX_LEVEL_2, interval2.getTaxLevel());
	}

}
