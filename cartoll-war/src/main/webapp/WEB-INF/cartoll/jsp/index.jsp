<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/resources/styles/reset.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/styles/main.css" />"
	rel="stylesheet">
<title>Home</title>
</head>
<body>
	<div class="container">
		<jsp:include page="header.jsp"></jsp:include>
		<div class="content">
			<div id="stations">
				<div id="stationsList">
				<h1>Stations</h1>
					<c:choose>
						<c:when test="${empty tollStationsBean}">
							<p>There are no stations available.</p>
						</c:when>
						<c:otherwise>
							<table class="stationTable">
								<tr>
									<th>Station</th>
									<th>No. of passes</th>
									<th>Tax</th>
								</tr>
								<c:forEach items="${tollStationsBean.tollStationBeans}" var="tollStationBean">
									<tr>
										<td><a href="<c:url value="/viewTollStation/${tollStationBean.tollStation.id}.html"/>">${tollStationBean.tollStation.name}</a></td>
										<td>${tollStationBean.statistic.passages}</td>
										<td>${tollStationBean.statistic.tax}</td>
									</tr>
								</c:forEach>
							</table>

						</c:otherwise>
					</c:choose>
				</div>
				<div id="stationsStatistics">
					<h1>Statistics</h1>
					<p>Total amount of passages:</p><span>${tollStationsBean.statistic.passages}</span>
					<p>Total amount of tax collected:</p><span>${tollStationsBean.statistic.tax}</span>
				</div>
			</div>
		</div>
	</div>
</body>
</html>